#Test for differentiation across groups, kegg modules


#only keep kegg modules that are in the map file, no useful interpretation possible otherwise

#-------------------------------------
#collapsed stool data:

ALPHA <- 0.1

test.xs <- list(otu=modulesc)

# different group comparisons
test.ixs <- list('HC v. IBS'=ixc.hc | ixc.ibs,
                 'HC v. IBSD'=ixc.hc | ixc.ibsd,
                 'HC v. IBSC'=ixc.hc | ixc.ibsc)
plot_by <- c("IBS", "Cohort", "Cohort")
col_list <- list(cols_ibs, cols_dh, cols_ch)


for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]

  # run all combinations of tests
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V5[temp_ind[1]])
    }
  }
    for(j in 1:length(test.ixs)){
        test.name <- names(test.ixs)[j]
        test.ix <- test.ixs[[j]]
        cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
        difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], mc[test.ix,plot_by[j]], parametric=FALSE)
        difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
        
        if(any(difftest.np$qvalues <= ALPHA)){
            signif.ix <- which(difftest.np$qvalues <= ALPHA)
            signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
            res <- data.frame(Module=colnames(test.x)[signif.ix],
                              qvalue=round(difftest.np$qvalues[signif.ix],5),
                              pvalue=round(difftest.np$pvalues[signif.ix],5),
                              round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                              log2FC = round(difftest.np$log2FC[signif.ix],5))
            res <- res[order(abs(res$log2FC), decreasing = T),]
            sink(sprintf('modules/diff_modules/differential_abundance_%s_%s.txt',x.name, test.name))
            write.table(res,row.names=F,sep='\t',quote=F)
            sink()
            pdf(sprintf('modules/diff_modules/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
            for(k in signif.ix){
              qval <- difftest.np$qvalues[k]
                working_mc <- subset(mc, test.ix)
                working_mc$vari <- data.transform(test.x)[test.ix,k]
       
                plot1 <- ggplot(working_mc, aes_string(x=plot_by[j], y= "vari")) +
                  geom_boxplot(outlier.shape = NA) +
                  geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
                  labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
                  theme(plot.title = element_text(size=10)) +
                  guides(fill=F, color=F) +
                  scale_y_continuous(trans='sqrt') +
                  scale_color_manual(values=col_list[[j]]) +
                  theme_cowplot(font_size = 10)
                plot(plot1)
            }
            dev.off()
            cat("see file\n")
        } else {
            cat("None\n")
        }
    }
}



######################################################################
#Test for flare differences, nothing significant <0.15

ALPHA <- 0.15

test.xs <- list(otu=modules)

# different group comparisons
IBS.flare <- m$IBS == "IBS" & !is.na(m$Flare)
IBS.Nflare <- m$IBS == "IBS" & is.na(m$Flare)
D.flare <- m$Cohort == "D" & !is.na(m$Flare)
D.Nflare <- m$Cohort == "D" & is.na(m$Flare)
C.flare <-  m$Cohort == "C" & !is.na(m$Flare)
C.Nflare <- m$Cohort == "C" & is.na(m$Flare)

test.ixs <- list('IBSflare v. IBSNF'= IBS.flare | IBS.Nflare,
                  'IBSDflare v. IBSDNF'=  D.flare | D.Nflare,
                 'IBSCflare v. IBSCNF'= C.flare | C.Nflare)

m$Flare <- as.character(m$Flare)
m$Flare[is.na(m$Flare)] <- "Baseline"
m$Flare <- factor(m$Flare)

plot_by <- c("Flare", "Flare", "Flare")
col_list <- list(cols_flare, cols_flare, cols_flare)

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V5[temp_ind[1]])
    }
  }
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
    if(any(difftest.np$qvalues <= ALPHA)){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Module=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      
      sink(sprintf('modules/diff_modules/differential_abundance_%s_%s.txt',x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('modules/diff_modules/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

######################################################################
#Test for Symptom severity differences

ALPHA <- 0.1

test.xs <- list(otu=modules)

# different group comparisons
IBS.SSSmild <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
IBS.SSSsevere <- m$IBS == "IBS" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
D.SSSmild <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
D.SSSsevere <- m$Cohort == "D" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
C.SSSmild <-  m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
C.SSSsevere <- m$Cohort == "C" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300
H.SSSmild <-  m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 < 300
H.SSSsevere <- m$Cohort == "H" & !is.na(m$IBS_symptom_severity_1_500) & m$IBS_symptom_severity_1_500 >= 300

table(D.SSSmild);table(D.SSSsevere);table(C.SSSmild);table(C.SSSsevere);table(H.SSSmild);table(H.SSSsevere)

m$SSS_binary <- "none_reported"
m$SSS_binary[m$IBS_symptom_severity_1_500 < 300] <- "mild"
m$SSS_binary[m$IBS_symptom_severity_1_500 >= 300] <- "severe"

test.ixs <- list('IBS.ssn_severe vs IBS.sss_mild' = IBS.SSSsevere | IBS.SSSmild,
                  'IBSD_ssn_severe v. IBSD_ssn_mild'=  D.SSSsevere | D.SSSmild,
                 'IBSCssn_severe v. IBSC_ssn_mild'= C.SSSsevere | C.SSSmild)

plot_by <- c("SSS_binary", "SSS_binary", "SSS_binary")
col_list <- list(rev(cols_flare), rev(cols_flare), rev(cols_flare))

# run all combinations of tests
for(i in 1:length(test.xs)){
  x.name <- names(test.xs)[i]
  test.x <- test.xs[[i]]
  
  for(k in 1:ncol(test.x)){
    temp_ind <- which(k_map$V1 == colnames(test.x)[k])
    if(length(temp_ind > 0)){
      colnames(test.x)[k] <- paste(k_map$V5[temp_ind[1]])
    }
  }
  
  for(j in 1:length(test.ixs)){
    test.name <- names(test.ixs)[j]
    test.ix <- test.ixs[[j]]
    cat(sprintf('Significant q-values for %s, %s:\n',x.name, test.name))
    difftest.np <- differentiation.test(data.transform(test.x)[test.ix,], m[test.ix,plot_by[j]], parametric=FALSE)
    difftest.np$qvalues[is.na(difftest.np$qvalues)] <- 1
    if(any(difftest.np$qvalues <= ALPHA)){
      signif.ix <- which(difftest.np$qvalues <= ALPHA)
      signif.ix <- signif.ix[order(difftest.np$pvalues[signif.ix])]
      res <- data.frame(Module=colnames(test.x)[signif.ix],
                        qvalue=round(difftest.np$qvalues[signif.ix],5),
                        pvalue=round(difftest.np$pvalues[signif.ix],5),
                        round(difftest.np$classwise.means[signif.ix,,drop=F],5),
                        log2FC = round(difftest.np$log2FC[signif.ix],5))
      res <- res[order(abs(res$log2FC), decreasing = T),]
      sink(sprintf('modules/diff_modules/differential_abundance_%s_%s.txt',x.name, test.name))
      write.table(res,row.names=F,sep='\t',quote=F)
      sink()
      pdf(sprintf('modules/diff_modules/differential_abundance_%s_%s.pdf',x.name, test.name),width=4,height=4)
      for(k in signif.ix){
        qval <- difftest.np$qvalues[k]
        working_m <- subset(m, test.ix)
        working_m$vari <- data.transform(test.x)[test.ix,k]
        #pdf(file=sprintf('diff_taxa/differential_abundance_%s_%s.pdf',x.name, test.name))
        plot1 <- ggplot(working_m, aes_string(x=plot_by[j], y= "vari")) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(position=position_jitter(0.1), size=3, alpha=0.75, aes_string(color=plot_by[j])) +
          #theme(legend.position = 'bottom') + 
          labs(x="", y= "Relative Abundance (sq rt)", title =  colnames(test.x)[k]) +
          scale_x_discrete(labels=c("SSS <300", "SSS >300")) +
          theme(plot.title = element_text(size=10)) +
          guides(fill=F, color=F) +
          scale_y_continuous(trans='sqrt') +
          scale_color_manual(values=col_list[[j]][c(2,1)]) +
          theme_cowplot(font_size = 10)
        plot(plot1)
      }
      dev.off()
      cat("see file\n")
    } else {
      cat("None\n")
    }
  }
}

######################################################################
