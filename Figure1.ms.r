# Figure 1 - Taxa and Kegg Beta Diversity PCoA, Alpha-Time, Beta-Time
# and within person variability

#201907 updated to:
#1) include percent variance explained
#2) not stratify by sex for luminal samples


######
# Filter the data
######


#Do not include flares for luminal samples, and only women for biopsies
#w_samples <- m$SampleID[as.character(m$Gender) == "F"]
w_samples <- m$SampleID[as.character(m$Gender) %in% c("F", "M")]
w_sample_bio <- m_bio$SampleID[as.character(m_bio$Gender)=="F"]

m <- m[w_samples,] #metadata for all samples are in m
flares <- m[m$Flare=="Flare" & ! is.na(m$Flare),"SampleID"]
xF<- x[flares,]
mF <- m[flares,]
x <- x[w_samples,] #all luminal data is in x
kegg <- kegg[w_samples,]
x_bio <- x_bio[w_sample_bio,] #all biopsy data is in x_bio, expanded with PCo
m_bio <- m_bio[w_sample_bio,]

noF <- m[is.na(m$Flare), "SampleID"]
m_noF <- m[noF,]
kegg_noF <- kegg[noF,]
x_noF <- x[noF,]


#---------------------------------------------------

#Calculate Alpha Div for all
alpha_div <- as.data.frame(diversity(x_noF, index="shannon"))
colnames(alpha_div) <- "shannon"
alpha_div$simpson <- diversity(x_noF, index="simpson")
alpha_div$obs_species <- rowSums(x_noF > 0)
alpha_div <- alpha_div[rownames(m_noF),]
m_noF$shannon <- alpha_div$shannon
m_noF$observed_species <- alpha_div$obs_species
m_noF$simpson <- alpha_div$simpson

#Alpha Div for collapsed
alpha_divC <- as.data.frame(diversity(xc, index="shannon"))
colnames(alpha_divC) <- "shannon"
alpha_divC$simpson <- diversity(xc, index="simpson")
alpha_divC$obs_species <- rowSums(xc > 0)
alpha_divC <- alpha_divC[rownames(mc),]
mc$shannon <- alpha_divC$shannon
mc$observed_species <- alpha_divC$obs_species
mc$simpson <- alpha_divC$simpson

#Alpha Dive for flares
alpha_divF <- as.data.frame(diversity(xF, index="shannon"))
colnames(alpha_divF) <- "shannon"
alpha_divF$simpson <- diversity(xF, index="simpson")
alpha_divF$obs_species <- rowSums(xF > 0)
alpha_divF <- alpha_divF[rownames(mF),]
mF$shannon <- alpha_divF$shannon
mF$observed_species <- alpha_divF$obs_species
mF$simpson <- alpha_divF$simpson

#Alpha Div for biopsies
alpha_divB <- as.data.frame(diversity(x_bio, index="shannon"))
colnames(alpha_divB) <- "shannon"
alpha_divB$simpson <- diversity(x_bio, index="simpson")
alpha_divB$obs_species <- rowSums(x_bio > 0)
alpha_divB <- alpha_divB[rownames(m_bio),]
m_bio$shannon <- alpha_divB$shannon
m_bio$observed_species <- alpha_divB$obs_species
m_bio$simpson <- alpha_divB$simpson

##################################################


######
# PCoA of taxa
######

set.seed(42)

#Beta div calc.

beta_div <- as.matrix(vegdist(x_noF, method = "bray"))

#Adonis to check differences in centroid, significantly different dispersion
beta_dist = as.dist(beta_div)
ad = adonis(beta_div ~ m_noF[,"Cohort"], data=m, permutations=1999)
p_val <- ad$aov.tab[1,6]
r_sq <- ad$aov.tab[1,5]

#Run Stats for diff. dispersion
beta_out <- betadisper(beta_dist, m_noF$Cohort)
p_val_disp <- permutest(beta_out)$tab[1, 6]

var_expl <- pcoa(beta_div)$values$Relative_eig *100
PCOA <- pcoa(beta_div)$vectors
colnames(PCOA) <- paste("PC",1:ncol(PCOA), sep="")

PCOA <- cbind(PCOA, rownames(PCOA))
colnames(PCOA)[ncol(PCOA)] <- "SampleID"
mapping2 <- m_noF
mapping2 <- data.frame(lapply(mapping2, as.character), stringsAsFactors=FALSE)
PCOA <- merge(PCOA, mapping2, by="SampleID")

PCOA$PC1 <- as.numeric(levels(PCOA$PC1))[PCOA$PC1]
PCOA$PC2 <- as.numeric(levels(PCOA$PC2))[PCOA$PC2]
PCOA$PC3 <- as.numeric(levels(PCOA$PC3))[PCOA$PC3]
PCOA$PC4 <- as.numeric(levels(PCOA$PC4))[PCOA$PC4]

range(PCOA$PC1)
range(PCOA$PC2)

centroids <- aggregate(cbind(PCOA$PC1,PCOA$PC2) ~ PCOA$Cohort,PCOA,mean)
colnames(centroids) <- c("Cohort", "PC1", "PC2")

#pairwise adonis for differences: all pairs are significantly differently dispersed with small R2
pair_ad_list <- list()
for(t in 1:2){
  for(u in (t+1):3){
    t1 <- unique(m$Cohort)[[t]]
    t2 <- unique(m$Cohort)[[u]]
    sams <- rownames(m_noF[m_noF$Cohort == t1 | m_noF$Cohort == t2,])
    beta_dist = beta_div[sams,sams]
    ad = adonis(beta_dist ~ m[sams,"Cohort"], data=m, permutations=1999)
    p_val2 <- ad$aov.tab[1,6]
    r_sq2 <- ad$aov.tab[1,5]
    pair_ad_list[[paste(t1, "_", t2, sep="")]] <- c("pval", p_val2, "r_sq", r_sq2)
  }
}

pc1_lab <- paste("PC1 ", round(var_expl[1], digits=1), "%", sep="")
pc2_lab <- paste("PC2 ", round(var_expl[2], digits=1), "%", sep="")

#do not add the stats since it is not collapsed data
OTU_PCoA <- ggplot(PCOA) +
  geom_point(size = 2.5, aes_string(x = "PC1", y = "PC2", color = "Cohort", alpha=0.25)) + 
  scale_color_manual(values=sel_colors) +
  guides(color=guide_legend(nrow=3), alpha=F) +
  theme(legend.title=element_blank()) +
  stat_chull(aes(x=PC1,y=PC2,color=Cohort, fill=Cohort),alpha = 0.01, geom = "polygon") +
  scale_fill_manual(values=sel_colors) +
  #annotate("text", x=-0.3, y=0.4, label= paste("P=", p_val), size=3.5) +
  #annotate("text", x=-0.3, y=0.45, label= paste("R2=", round(r_sq, digits=3)), size=3.5) +
  labs(x= pc1_lab, y= pc2_lab) +
  theme_cowplot()
  #geom_point(data=centroids, aes(x=PC1, y=PC2, fill=Cohort), 
  #           shape = 23, size=3, alpha=0.8, show.legend = F)

#################################################
#################################################
#finding most extreme samples from this plot based on PC2 coordinates; note no flare samples in here
#identified by laying beyond the most exteme HC sample

#test whether these have differences in metabolite levels, using the aggregated scaled metabolite file
#PC2 location does not seem to correlate with BCDI by visual inspection


PCOA_PC2_df <- PCOA[,c("PC2", "Cohort", "study_id", "unique_id")]
#get Healthy extremes
PC2_H_range <- range(PCOA_PC2_df$PC2[PCOA_PC2_df$Cohort == "H"])

PCOA_PC2_df_extremes_D <- PCOA_PC2_df[PCOA_PC2_df$PC2 > max(PC2_H_range),] #most 10007567 are in here
PCOA_PC2_df_extremes_C <- PCOA_PC2_df[PCOA_PC2_df$PC2 < min(PC2_H_range),]

#save(PCOA_PC2_df_extremes_D, file="PCOA_PC2_df_extremes_D.Rdata")
#save(PCOA_PC2_df_extremes_C, file="PCOA_PC2_df_extremes_C.Rdata")

#did this once; no need to run code every time

#add column to original version of metadata
#setwd("/Users/m210320/Dropbox/IBS/Metadata_Files")
#sample_name <- "Final_Cleaned_Condensed_Metadata.csv"
#all_meta <- as.data.frame(read.csv(sample_name), stringsAsFactors=F)
#make all meta unique_id
#all_meta$unique_id <- paste(all_meta$study_id, all_meta$Timepoint, sep = "_") #flare ones are not correct but that is fine

#all_meta$extreme_pcoa <- "normal"
#all_meta$extreme_pcoa[all_meta$unique_id %in% PCOA_PC2_df_extremes_D$unique_id] <- "extreme"
#all_meta$extreme_pcoa[all_meta$unique_id %in% PCOA_PC2_df_extremes_C$unique_id] <- "extreme"

#table(all_meta$extreme_pcoa) #26, 448
#write.csv(all_meta, "test_meta.csv") #manually combined




#################################################
#################################################
######
# Use Collapsed Data
#####

set.seed(42)

#Beta div calc.
beta_div_c <- as.matrix(vegdist(xc, method = "bray"))

#Adonis to check differences in centroid, significantly different dispersion
#squared deviations of each of sample to the centroid
beta_dist = as.dist(beta_div_c)
ad = adonis(beta_div_c ~ mc[,"Cohort"], data=mc, permutations=1999)
p_val <- ad$aov.tab[1,6]
r_sq <- ad$aov.tab[1,5]


#Run Stats for diff. dispersion, significantly different dispersion from all samples lost
beta_out <- betadisper(beta_dist, mc$Cohort)
p_val_disp <- permutest(beta_out)$tab[1, 6]
var_expl <- pcoa(beta_div_c)$values$Relative_eig *100

PCOA <- pcoa(beta_div_c)$vectors
colnames(PCOA) <- paste("PC",1:ncol(PCOA), sep="")

PCOA <- cbind(PCOA, mc$SampleID)
colnames(PCOA)[ncol(PCOA)] <- "SampleID"
mapping2 <- mc
mapping2 <- data.frame(lapply(mapping2, as.character), stringsAsFactors=FALSE)
PCOA <- merge(PCOA, mapping2, by="SampleID")

PCOA$PC1 <- as.numeric(levels(PCOA$PC1))[PCOA$PC1]
PCOA$PC2 <- as.numeric(levels(PCOA$PC2))[PCOA$PC2]
PCOA$PC3 <- as.numeric(levels(PCOA$PC3))[PCOA$PC3]
PCOA$PC4 <- as.numeric(levels(PCOA$PC4))[PCOA$PC4]

range(PCOA$PC1)
range(PCOA$PC2)

centroids <- aggregate(cbind(PCOA$PC1,PCOA$PC2) ~ PCOA$Cohort,PCOA,mean)
colnames(centroids) <- c("Cohort", "PC1", "PC2")

table(mc$Cohort)

#pairwise adonis for differences: all pairwise PERMANOVA comparisons <0.05, C-D = 0.001
pair_ad_list <- list()
for(t in 1:2){
  for(u in (t+1):3){
    t1 <- unique(m$Cohort)[[t]]
    t2 <- unique(m$Cohort)[[u]]
    sams <- rownames(mc[mc$Cohort == t1 | mc$Cohort == t2,])
    beta_dist = beta_div_c[sams,sams]
    ad = adonis(beta_dist ~ mc[sams,"Cohort"], data=mc, permutations=1999)
    p_val2 <- ad$aov.tab[1,6]
    r_sq2 <- ad$aov.tab[1,5]
    pair_ad_list[[paste(t1, "_", t2, sep="")]] <- c("pval", p_val2, "r_sq", r_sq2)
  }
}


pc1_lab <- paste("PC1 ", round(var_expl[1], digits=1), "%", sep="")
pc2_lab <- paste("PC2 ", round(var_expl[2], digits=1), "%", sep="")

OTU_C_PCoA <- ggplot(PCOA) +
  geom_point(size = 2.5, aes_string(x = "PC1", y = "PC2", color = "Cohort", alpha=0.25)) + 
  scale_color_manual(values=sel_colors) +
  guides(color=guide_legend(nrow=3), alpha=F) +
  theme(legend.title=element_blank()) +
  stat_chull(aes(x=PC1,y=PC2,color=Cohort, fill=Cohort),alpha = 0.01, geom = "polygon") +
  scale_fill_manual(values=sel_colors) +
  annotate("text", x=-0.21, y=0.31, label= paste("P=", p_val), size=3.5) +
  annotate("text", x=-0.21, y=0.35, label= paste("R2=", round(r_sq, digits=3)), size=3.5) +
  labs(x= pc1_lab, y= pc2_lab) +
  theme_cowplot()
#geom_point(data=centroids, aes(x=PC1, y=PC2, fill=Cohort), 
#           shape = 23, size=3, alpha=0.8, show.legend = F)



#####
# Plot between vs within
####

#need sampleIDs as rownames so reinitiate xc dataframe

xc <- t(x_RA_collapsed_tax_list_woF_sub$species[,-1])
colnames(xc) <- x_RA_collapsed_tax_list_woF_sub$species$taxa
rownames(xc) <- mc$SampleID

beta_div_c <- as.matrix(vegdist(xc, method = "bray"))


# get all within-cohort distances, per subject
distance_table <- data.frame("Subject"=NA, "Variability"=NA, "Comparison"=NA)
mc$Cohort <- as.character(mc$Cohort)
people <- sort(unique(mc$SampleID))

for(i in 1:length(people)){
  cohort <- mc[mc$SampleID == people[i],"Cohort"] #select cohort people belongs to
  withins <- mc[!mc$SampleID == people[i] & mc$Cohort == cohort,"SampleID"] #get all other subjects from cohort
  within_name <- paste("within_", cohort, sep ="")
  dist_within <- mean(beta_div_c[withins, people[i]])
  distance_table <- rbind(distance_table, c(people[i], dist_within, within_name))
  
  non <- unique(mc$Cohort)[!unique(mc$Cohort) == cohort]
  for(j in 1:length(non)){
    name <- paste(cohort, "_", non[j], sep ="")
    betweens <-  mc[!mc$SampleID == people[i] & mc$Cohort == non[j],"SampleID"] #all subjects from cohort
    dist_btwn <- mean(beta_div_c[betweens,people[i]]) #distance of that persons sample with all from other cohort
    distance_table <- rbind(distance_table, c(people[i], dist_btwn, name))
  }
}

distance_table <- distance_table[2:nrow(distance_table),]
distance_table$Variability <- as.numeric(as.character(distance_table$Variability))
distance_table[distance_table$Comparison == "D_C", "Comparison"] <- "C_D" 
distance_table[distance_table$Comparison == "D_H", "Comparison"] <- "H_D" 
distance_table[distance_table$Comparison == "C_H", "Comparison"] <- "H_C" 

keep_dists <- c("within_C", "H_C", "within_H", "H_D", "within_D")
distance_table <- distance_table[distance_table$Comparison %in% keep_dists,]
distance_table$Comparison <- factor(distance_table$Comparison, 
                                    levels = c("within_C", "H_C", "within_H",
                                               "H_D", "within_D"))

#anova, more power and more appropriate than the pairwise t test that Tonya did
fit <- aov(distance_table$Variability ~ distance_table$Comparison)
aov_res <- TukeyHSD(fit, conf.level=0.99, ordered=F)

table(distance_table$Comparison)
#within_C      H_C    within_H      H_D     within_D 
#22       46       24       53       29 

#diff         lwr          upr     p adj
#H_D-H_C           -0.022222504 -0.05764645  0.013201438 0.2330384
#within_C-H_C       0.003591288 -0.03854892  0.045731494 0.9985773
#within_D-H_C      -0.049182419 -0.09275272 -0.005612115 0.0024083
#within_H-H_C      -0.014533385 -0.05810369  0.029036918 0.8019419
#within_C-H_D       0.025813792 -0.01670647  0.068334055 0.2636024
#within_D-H_D      -0.026959915 -0.07089791  0.016978077 0.2535270
#within_H-H_D       0.007689118 -0.03624887  0.051627111 0.9776060
#within_D-within_C -0.052773708 -0.10228622 -0.003261199 0.0049208
#within_H-within_C -0.018124674 -0.06763718  0.031387834 0.7419674
#within_H-within_D  0.034649034 -0.01608619  0.085384257 0.1615940


# Pairwise test ttest like implemented before
pval_table <- data.frame("Test_Name"=NA, "Pval"=NA)
data_list_pairwise <- list()
for(i in 1:(length(unique(distance_table$Comparison))-1)){
  for(j in (i+1):length(unique(distance_table$Comparison))){
    name1 <- unique(as.character(distance_table$Comparison))[i]
    name2 <- unique(as.character(distance_table$Comparison))[j]
    test_name <- paste(name1,"_vs_",name2, sep="")
    group1_values <- distance_table[as.character(distance_table$Comparison)==name1,"Variability"]
    group2_values <-distance_table[as.character(distance_table$Comparison)==name2,"Variability"]
    test.p <- t.test(group1_values, group2_values)$p.val
    pval_table <- rbind(pval_table, c(test_name, test.p))
    }
}


Distance_Collapsed <- ggplot(distance_table, aes(x=Comparison, y=Variability)) +
  geom_jitter(width=0.09, aes(color=Comparison), alpha=0.65, size=2.5) +
  geom_boxplot(fill=NA, outlier.color = NA) +
  scale_color_manual(values=c(sel_colors[1], cols[4], sel_colors[3], cols[3], sel_colors[2])) +
  labs(y = "Within-subject mean BC distance") +
  guides(color=F) +
  theme_cowplot()

##### 
# Plot Diversity by Time
#####
# get all within-patient distances
vari_table <- data.frame("Subject"=NA, "Timepoint"=NA, "Cohort"=NA, "Distance"=NA)
m_noF$ID_on_tube <- as.numeric(m_noF$ID_on_tube)
patient.nos <- sort(unique(m_noF$ID_on_tube))
for(i in 1:length(unique(m_noF$ID_on_tube))){
  patient.no <- patient.nos[i]
  sub_map <- m_noF[m_noF$ID_on_tube == patient.no,]
  sub_map2 <- sub_map[with(sub_map,order(sub_map$Timepoint)),]
  
  cohort <- sub_map$Cohort[1] 
  
  # calculates only distance to previous timepoint
  if(nrow(sub_map2)> 1){
    for(j in 1:(nrow(sub_map2)-1)){
      timepoint <- sub_map2[j,"Timepoint"]
      distance <- beta_div[sub_map2[j,"SampleID"], sub_map2[j+1, "SampleID"]]
      vari_table <- rbind(vari_table, c(patient.no, timepoint, cohort,distance))
    }
  }
}
vari_table <- vari_table[2:nrow(vari_table),]
vari_table$Distance <- as.numeric(as.character(vari_table$Distance))

Variability <- ggplot(vari_table, aes(x=Timepoint, y=Distance, group=Cohort, color=Cohort)) +
  geom_jitter(width=0.2, alpha=0.3, size=1.5, color="grey") +
  geom_smooth(alpha=0.25) +
  scale_color_manual(values=sel_colors) +
  labs(y = "Taxonomic Variability (Bray-Curtis)") +
  theme_cowplot()

empty_plot <- plot.new()

#Figure1 <- plot_grid(OTU_PCoA, Variability, OTU_C_PCoA, Distance_Collapsed, labels=c("A", "B", "C", "D"))
Figure1 <- plot_grid(Distance_Collapsed, empty_plot, OTU_PCoA, OTU_C_PCoA)


save_plot("Figure1.ms.png", Figure1,
          ncol = 2, # we're saving a grid plot of 2 columns
          nrow = 2, # and 2 rows
          # each individual subplot should have an aspect ratio of 1.3
          base_aspect_ratio = 1.2
)

pdf("distance_collapsed.pdf", height=3.5, width=3)
plot(Distance_Collapsed)
dev.off()




